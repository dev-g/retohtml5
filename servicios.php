<?php $titulo='Servicios' ?>
<?php $menu='servicios' ?>
<?php include 'header.php' ?>
<div id="container-bg">
    <div id="container">

        <dl  class="welcome">
            <dt class="h2" id="fotografia-eventos">
           Fotografía Para Eventos
            </dt>

            <dd>
                Brindamos el mejor servicio a nuestros clientes ofreciendo un servicio exclusivo y de calidad, para que nuestros clientes queden totalmente satisfechos en esas fechas que son tan especiales para ellos.
            </dd>

            <dt class="h2" id="fotografia-documental">
            Fotografía Documental
            </dt>

            <dd>
                Nos encargamos de capturar los mejores momentos con la mejor calidad, utilizando cámaras de última generación para obtener una calidad inigualable de imagen que permita mostrar todo el esplendor de su documental.
            </dd>

            <dt class="h2" id="fotografia-callejera">
              Fotografía Callejera
            </dt>

            <dd>
                El arte esta en las calles, y es donde nosotros lo capturamos, contamos con el mejor equipo técnico, capacitado para tomar registro de todo esa magia que encontramos en las calles.
            </dd>

            <dt class="h2" id="fotografia-reportajes">
           Fotografía Para Reportajes
            </dt>

            <dd>
                Los reportajes son la mejor presentación de un tema de importancia a las masas, es por eso que es aquí donde el mejor equipo humano con el que contamos saca a flote todas sus habilidades y capacidades que permitan obtener las mejores imágenes para tus reportajes, simplemente un servicio integral.
            </dd>

            <dt class="h2" id="fotografia-deportiva">
            Fotografía Deportiva
            </dt>

            <dd>
                Nos encargamos de capturar esos momentos que llenan de emoción a todos para que luego puedas recordarlos no solo con emoción sino con una calidad de imagen impresionante que te ara recordar lo mejor de esos momentos inolvidables. 
            </dd>

            <dt class="h2" id="fotografia-naturaleza">
            Fotografía a la Naturaleza
            </dt>

            <dd>
                Solo hay una forma de llevar la naturaleza a todas partes y esa es atreves de una fotografía, tú dinos donde y nosotros nos encargamos de capturar el mejor angulo.  
            </dd>

            <dt class="h2" id="fotografia-paisajista">
            Fotografía Paisajista
            </dt>

            <dd>
                Solo hay una forma de recordar un paisaje y esa es atreves de una buena fotografía, tú dinos donde y nosotros nos encargamos de capturar lo mejor del paisaje.   
            </dd>


        </dl>


    </div>
</div>
<?php include 'footer.php' ?>