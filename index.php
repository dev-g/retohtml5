<?php $titulo='Inicio' ?>
<?php $menu='inicio' ?>
    <?php include 'header.php' ?>
<div id="container-bg">
    <div id="container">
        <div class="welcome">
            <p>
                Bienvenidos a <strong>Photo Look</strong> una empresa dedicada a la Fotografía digital, damos servicio de Fotografía de distintos tipos: Para Eventos, Documental, Callejera, Para Reportajes, Deportiva, a la Naturaleza y Paisajista.
            </p>
        </div>
        <div class="portal">
            <div class="slide">
                <div class="info">

                    <a href="portafolio/2011/coros-navidenos.php" title="Ver más Fotografías del Evento"><h2>Evento "Concurso de Coros Navideños 2011"</h2></a>
                    <em>Fotografía para Eventos</em>
                    <p> El III Concurso de Coros Navideños “Davillancicos 2011”, en su etapa final, contó con la activa participación de alumnos, docentes y colaboradores administrativos...</p>
                    <a href="portafolio/2011/coros-navidenos.php" class="button-black">Ver Mas</a>
                </div>
                <div class="background"></div>

                <img src="images/front/coros-navidenos.jpg" alt="Imagen de Portada del Evento Coros Navideños" />
            </div>
        </div>
        <div class="module">
            <h3>Frase del Dia</h3>
            <div class="cite">
                <em class="left">&#8220;</em>
                <p>
                    Siempre pensé que las buenas fotos son como las buenas bromas, si se tienen que explicar es que no eran tan buenas.
                </p>
                <em class="right">&#8221;</em>
                <div class="autor">
                    por <strong>Anónimo</strong>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>