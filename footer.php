<hr/>
<div id="footer-bg">
    <div id="footer">
        <div class="modules">
            <div class="module medium">
                <div class="title">
                    <span class="icon twitter"></span>
                    <h3>Novedades en Twitter</h3>
                </div>
                <!--
                ya no usare dialog por que al parecer se elimino de html5
                <dialog class="container">
                -->
                <dl class="container">
                    <dt>@photolook3:</dt>
                    <dd  class="tweet">
                        <p>
                            Listo la web ya esta lista, Ahora ya puedes ver nuestros Trabajos
                        </p>
                        <em class="ago">Hace 6 Minutos</em>
                    </dd>
                    <dt>@photolook3:</dt>
                    <dd  class="tweet">
                        <p>
                            Pronto lanzaremos la web oficial estean Atentos
                        </p>
                        <em class="ago">Hace 9 Minutos</em>
                    </dd>
                    <dt>@photolook3:</dt>
                    <dd  class="tweet">
                        <p>
                            Bienvenidos a #PhotoLook el Mejor Servicio de Fotografía Digital
                        </p>
                        <em class="ago">Hace 13 Minutos</em>
                    </dd>
                </dl>
                <!--
                </dialog>
                -->
            </div>
            <div class="module medium">
                <div class="title">
                    <span class="icon jobs"></span>
                    <h3>Ultimos Trabajos</h3>
                </div>
                <div class="container">
                    <ul class="mini-galery">
                        <li>
                            <a href="/portafolio/2011/coros-navidenos.php" title="Concurso de Coros Navideños Da Vinci 2011">
                                <img src="/images/front/last/1.png" alt="imagen" />
                            </a>
                        </li>

                        <li>
                            <a href="/portafolio/2011/copa-davinci.php" title="Copa Da Vinci 2011">
                                <img src="/images/front/last/2.png" alt="imagen" />
                            </a>
                        </li>
                        <li>
                            <a href="/portafolio/2011/mis-y-mister-davinci.php" title="Mis y Mister Da Vinci 2011">
                                <img src="/images/front/last/3.png" alt="imagen" />
                            </a>
                        </li>

                        <li>
                            <a href="/portafolio/2010/un-jugete-una-sonrisa.php" title="Regala un Juguete, Regala una Sonrisa">
                                <img src="/images/front/last/4.png" alt="imagen" />
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="module">
                <div class="title">
                    <span class="icon links"></span>
                    <h3>Enlaces</h3>
                </div>
                <div class="container">
                    <ul class="links">
                        <li>
                            <a href="http://aulaazure.ti-capacitacion.com/" target="_blank">
                                <span>Certificación HTML5</span>
                            </a>
                        </li>
                        <li>
                            <a href="http://validator.w3.org/check/referer" target="_blank">
                                <span>Validación HTML</span>
                            </a>
                        </li>
                        <li>
                            <a href="http://jigsaw.w3.org/css-validator/check/referer" target="_blank">
                                <span>Validación CSS</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="module">
                <div class="title">
                    <span class="icon services"></span>
                    <h3>Servicios</h3>
                </div>
                <div class="container">
                    <ul class="services">
                        <li>
                            <a href="/servicios.php">
                                <span>Fotografía</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="/servicios.php#fotografia-eventos">
                                        <span>Para Eventos</span>
                                    </a>
                                </li>    
                                <li>
                                    <a href="/servicios.php#fotografia-documental">
                                        <span>Documental</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/servicios.php#fotografia-callejera">
                                        <span>Callejera</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/servicios.php#fotografia-reportajes">
                                        <span>Para Reportajes</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/servicios.php#fotografia-deportiva">
                                        <span>Deportiva</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/servicios.php#fotografia-naturaleza">
                                        <span>a la Naturaleza</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/servicios.php#fotografia-paisajista">
                                        <span>Paisajista</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <a href="#header" class="right"> Ir a la Parte Superior</a>

        <div id="creditos">
            &copy; Todos Los Derechos Reservados a <a href="mailto:jeral17@gmail.com">Gerson Alexander Pardo Gamez</a> | 
            W3C standart Sitio Web valido <a target="_blank" href="http://validator.w3.org/check/referer">HTML</a> y  
            <a target="_blank" href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>
            <br/>
            <a target="_blank" href="http://jigsaw.w3.org/css-validator/check/referer">
                <img style="border:0;width:88px;height:31px"
                     src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
                     alt="¡CSS Válido!" />
            </a>
        </div>
    </div>
</div>
<!-- script compiler -->
<script src="/compiler/less.min.js"></script>
<!-- end compiler-->
</body>
</html>
