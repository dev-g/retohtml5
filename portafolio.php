<?php $titulo='Portafolio' ?>
<?php $menu='portafolio' ?>
<?php include 'header.php' ?>
<div id="container-bg">
    <div id="container">

        <h2 id="del-2011">2011</h2>
        <ul class="portfolio">
            <li>
                <a href="portafolio/2011/coros-navidenos.php">
                    <img src="/images/portfolio/1.png" alt="Concurso de Coros Navideños" />
                    <span>Concurso de Coros Navideños</span>
                </a>
            </li>
            <li>
                <a href="portafolio/2011/copa-davinci.php">
                    <img src="/images/portfolio/2.png" alt="Copa Da Vinci" />
                    <span>Copa Da Vinci</span>
                </a>
            </li>
            <li>
                <a href="portafolio/2011/mis-y-mister-davinci.php">
                    <img src="/images/portfolio/3.png" alt="Mis y Mister Da Vinci" />
                    <span>Miss & Mister Da Vinci</span>
                </a>
            </li>

        </ul>

        <h2 id="del-2010">2010</h2>
        <ul class="portfolio">
        
            <li>
                <a href="portafolio/2010/un-jugete-una-sonrisa.php">
                    <img src="/images/portfolio/4.png" alt="Regala un Juguete, Regala una Sonrisa" />
                    <span>Regala un Juguete, Regala una Sonrisa</span>
                </a>
            </li>
             <li>
                <a href="portafolio/2010/concurso-coreografias.php">
                    <img src="/images/portfolio/5.png" alt="Concurso de Coreografías Da Vinci" />
                    <span>Concurso de Coreografías Leonardo Da Vinci</span>
                </a>
            </li>

        </ul>

        

    </div>
</div>
<?php include 'footer.php' ?>