<?php $titulo = 'Regala un Juguete, Regala una Sonrisa 2010 | Portafolio' ?>
<?php $menu = '' ?>
<?php include '../../header.php' ?>
<div id="container-bg">
    <div id="container">
        <a href="../../portafolio.php#del-2010"> &laquo; Volver al Portafolio</a>
        <h2>Regala un Juguete, Regala una Sonrisa 2010</h2>
        <div class="welcome">
            <p>
                Fotografías realizadas para la Campaña "Regala un Juguete, Regala una Sonrisa" Auspiciada por el Instituto Leonardo Da Vinci.
            </p>
        </div>
        <h3>Algunas Fotos:</h3>
        <ul class="portfolio">
            <li>
                <a target="_blank" href="/images/portfolio/juguete-sonrisa/1.jpg">
                    <img src="/images/portfolio/juguete-sonrisa/1.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/juguete-sonrisa/2.jpg">
                    <img src="/images/portfolio/juguete-sonrisa/2.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/juguete-sonrisa/3.jpg">
                    <img src="/images/portfolio/juguete-sonrisa/3.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/juguete-sonrisa/4.jpg">
                    <img src="/images/portfolio/juguete-sonrisa/4.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/juguete-sonrisa/5.jpg">
                    <img src="/images/portfolio/juguete-sonrisa/5.jpg" alt="Imagen del Evento" />

                </a>
            </li>


        </ul>

        <a href="../../portafolio.php#del-2010"> &laquo; Volver al Portafolio</a>


    </div>
</div>
<?php include '../../footer.php' ?>