<?php $titulo = 'Concurso de Coreografias 2010 | Portafolio' ?>
<?php $menu = '' ?>
<?php include '../../header.php' ?>
<div id="container-bg">
    <div id="container">
        <a href="../../portafolio.php#del-2010"> &laquo; Volver al Portafolio</a>
        <h2>Concurso de Coreografias 2010</h2>
        <div class="welcome">
            <p>
                Fotografías realizadas para el III Concurso de Coreografias del instituto Leonardo Da Vinci.
            </p>
        </div>
        <h3>Algunas Fotos:</h3>
        <ul class="portfolio">
            <li>
                <a target="_blank" href="/images/portfolio/concurso-coreografias/1.jpg">
                    <img src="/images/portfolio/concurso-coreografias/1.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/concurso-coreografias/2.jpg">
                    <img src="/images/portfolio/concurso-coreografias/2.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/concurso-coreografias/3.jpg">
                    <img src="/images/portfolio/concurso-coreografias/3.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/concurso-coreografias/4.jpg">
                    <img src="/images/portfolio/concurso-coreografias/4.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/concurso-coreografias/5.jpg">
                    <img src="/images/portfolio/concurso-coreografias/5.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/concurso-coreografias/6.jpg">
                    <img src="/images/portfolio/concurso-coreografias/6.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/concurso-coreografias/7.jpg">
                    <img src="/images/portfolio/concurso-coreografias/7.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/concurso-coreografias/8.jpg">
                    <img src="/images/portfolio/concurso-coreografias/8.jpg" alt="Imagen del Evento" />

                </a>
            </li>

            <li>
                <a target="_blank" href="/images/portfolio/concurso-coreografias/9.jpg">
                    <img src="/images/portfolio/concurso-coreografias/9.jpg" alt="Imagen del Evento" />

                </a>
            </li>

        </ul>

        <a href="../../portafolio.php#del-2010"> &laquo; Volver al Portafolio</a>


    </div>
</div>
<?php include '../../footer.php' ?>