<?php $titulo = 'Copa Da Vinci 2011 | Portafolio' ?>
<?php $menu = '' ?>
<?php include '../../header.php' ?>
<div id="container-bg">
    <div id="container">
        <a href="../../portafolio.php#del-2011"> &laquo; Volver al Portafolio</a>
        <h2>Copa Da Vinci 2011</h2>
        <div class="welcome">
            <p>
                Fotografías realizadas para la Copa Da Vinci 2011 - II, un dia Deportivo con muchos momentos que capturar.
            </p>
        </div>
        <h3>Algunas Fotos:</h3>
        <ul class="portfolio">
            <li>
                <a target="_blank" href="/images/portfolio/copa-davinci/1.jpg">
                    <img src="/images/portfolio/copa-davinci/1.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/copa-davinci/2.jpg">
                    <img src="/images/portfolio/copa-davinci/2.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/copa-davinci/3.jpg">
                    <img src="/images/portfolio/copa-davinci/3.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/copa-davinci/4.jpg">
                    <img src="/images/portfolio/copa-davinci/4.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/copa-davinci/5.jpg">
                    <img src="/images/portfolio/copa-davinci/5.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/copa-davinci/6.jpg">
                    <img src="/images/portfolio/copa-davinci/6.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/copa-davinci/7.jpg">
                    <img src="/images/portfolio/copa-davinci/7.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/copa-davinci/8.jpg">
                    <img src="/images/portfolio/copa-davinci/8.jpg" alt="Imagen del Evento" />

                </a>
            </li>

            <li>
                <a target="_blank" href="/images/portfolio/copa-davinci/9.jpg">
                    <img src="/images/portfolio/copa-davinci/9.jpg" alt="Imagen del Evento" />

                </a>
            </li>

        </ul>

        <a href="../../portafolio.php#del-2011"> &laquo; Volver al Portafolio</a>


    </div>
</div>
<?php include '../../footer.php' ?>