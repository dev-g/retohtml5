<?php $titulo = 'Miss & Mister Da Vinci 2011 | Portafolio' ?>
<?php $menu = '' ?>
<?php include '../../header.php' ?>
<div id="container-bg">
    <div id="container">
        <a href="../../portafolio.php#del-2011"> &laquo; Volver al Portafolio</a>
        <h2>Miss & Mister Da Vinci 2011</h2>
        <div class="welcome">
            <p>
                Fotografías realizadas para el Concurso de la Miss y el Mister Da Vinci 2011.
                Momentos de mucha emoción se vivieron esta noche en la elección de los Ganadores.
                Ante la ovación del público las candidatas y candidatos desplegaron en todo momento seguridad, 
                espontaneidad y simpatía ante el público espectador que no dejaba de aplaudir a sus candidatos favoritos,
                fue el jurado calificador quién tuvo la difícil labor de elegir a la soberana Miss Da Vinci 2011 Srta. Diana Hidalgo Calle,
                que lució un elegante vestido de noche y traje de baño, lo que la convirtió en una de las más aplaudidas de la noche; 
                así como también al joven representante Mister Da Vinci 2011 Jonathan Portillo Vela, quién desplegó toda su seguridad y en esta noche de gala.
            </p>
        </div>
        <h3>Algunas Fotos:</h3>
        <ul class="portfolio">
            <li>
                <a target="_blank" href="/images/portfolio/mis-y-mister/1.jpg">
                    <img src="/images/portfolio/mis-y-mister/1.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/mis-y-mister/2.jpg">
                    <img src="/images/portfolio/mis-y-mister/2.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/mis-y-mister/3.jpg">
                    <img src="/images/portfolio/mis-y-mister/3.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/mis-y-mister/4.jpg">
                    <img src="/images/portfolio/mis-y-mister/4.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/mis-y-mister/5.jpg">
                    <img src="/images/portfolio/mis-y-mister/5.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/mis-y-mister/6.jpg">
                    <img src="/images/portfolio/mis-y-mister/6.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/mis-y-mister/7.jpg">
                    <img src="/images/portfolio/mis-y-mister/7.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/mis-y-mister/8.jpg">
                    <img src="/images/portfolio/mis-y-mister/8.jpg" alt="Imagen del Evento" />

                </a>
            </li>


        </ul>

        <a href="../../portafolio.php#del-2011"> &laquo; Volver al Portafolio</a>


    </div>
</div>
<?php include '../../footer.php' ?>