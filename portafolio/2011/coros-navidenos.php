<?php $titulo = 'Coros Navideños Da Vinci 2011 | Portafolio' ?>
<?php $menu = '' ?>
<?php include '../../header.php' ?>
<div id="container-bg">
    <div id="container">
        <a href="../../portafolio.php#del-2011"> &laquo; Volver al Portafolio</a>
        <h2>Coros Navideños Da Vinci 2011</h2>
        <div class="welcome">
            <p>
                Fotografías realizadas para el III Concurso de Coros Navideños “Davillancicos 2011”, 
                que llegó a su etapa final y contó con la activa participación de alumnos,
                docentes y colaboradores administrativos, quienes interpretaron diversos y alegres villancicos. 
                Las palabras de bienvenida e inauguración de este esperado concurso estuvieron a cargo del 
                Ing. Roberto Tejada - Director de Instituto, quien además agradeció la presencia de todos los asistentes.
            </p>
        </div>
        <h3>Algunas Fotos:</h3>
        <ul class="portfolio">
            <li>
                <a target="_blank" href="/images/portfolio/coros-navidenos/1.jpg">
                    <img src="/images/portfolio/coros-navidenos/1.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/coros-navidenos/2.jpg">
                    <img src="/images/portfolio/coros-navidenos/2.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/coros-navidenos/3.jpg">
                    <img src="/images/portfolio/coros-navidenos/3.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/coros-navidenos/4.jpg">
                    <img src="/images/portfolio/coros-navidenos/4.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/coros-navidenos/5.jpg">
                    <img src="/images/portfolio/coros-navidenos/5.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/coros-navidenos/6.jpg">
                    <img src="/images/portfolio/coros-navidenos/6.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/coros-navidenos/7.jpg">
                    <img src="/images/portfolio/coros-navidenos/7.jpg" alt="Imagen del Evento" />

                </a>
            </li>
            <li>
                <a target="_blank" href="/images/portfolio/coros-navidenos/8.jpg">
                    <img src="/images/portfolio/coros-navidenos/8.jpg" alt="Imagen del Evento" />

                </a>
            </li>

            <li>
                <a target="_blank" href="/images/portfolio/coros-navidenos/9.jpg">
                    <img src="/images/portfolio/coros-navidenos/9.jpg" alt="Imagen del Evento" />

                </a>
            </li>

        </ul>

        <a href="../../portafolio.php#del-2011"> &laquo; Volver al Portafolio</a>


    </div>
</div>
<?php include '../../footer.php' ?>