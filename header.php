<!DOCTYPE html>
<html>
    <head>
        <meta name="application-name" content="Photo Look"/>
        <meta name="msapplication-tooltip" content="Servicio de Fotograf&iacute;a Digital"/>
        <meta name="msapplication-task" content="name=Portafolio;action-uri=http://server203.elretohtml5.com/portafolio.html;icon-uri=http://server203.elretohtml5.com/portafolio.ico"/>
        <meta name="msapplication-task" content="name=Quienes Somos;action-uri=http://server203.elretohtml5.com/quienes.html;icon-uri=http://server203.elretohtml5.com/quienes.ico"/>
        <meta name="msapplication-task" content="name=Servicios;action-uri=http://server203.elretohtml5.com/servicios.html;icon-uri=http://server203.elretohtml5.com/servicios.ico"/>
        <meta name="msapplication-task" content="name=Contactar;action-uri=http://server203.elretohtml5.com/contactar.html;icon-uri=http://server203.elretohtml5.com/contactar.ico"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="description" content="Photo Look una empresa dedicada a la Fotografía digital, damos servicio de Fotografía de distintos tipos como Fotografía para Eventos, Fotografía Documental,Fotografía Callejera, Fotografía a Para Reportajes, Fotografía Deportiva, Fotografía a la Naturaleza, Fotografía Paisajista" />
        <meta name="keywords" content="photo,look,fotografia,digital,paisajita,fotos,fotografiar" />
        <meta name="author" content="Gerson Alexander Pardo Gamez"/>
        <title><?php echo isset($titulo) && !empty($titulo) ? $titulo : 'Photo Look' ?> | Photo Look - Fotografía Digital</title>
        <link rel="icon" type="image/x-icon" href="/favicon.ico" />
        <link rel="stylesheet" href="/less/main.css">
        <link rel="stylesheet/less" type="text/css" href="/less/main.less">
    </head>
    <body>
        <div id="header-bg">
            <div id="header">
                <div id="logo">
                    <h1>Photo Look</h1>
                </div>
                <ul id="menu">
                    <li<?php echo isset($menu) && $menu=='inicio' ? ' class="active"':'' ?>>
                        <a href="/index.php">
                            <span>Inicio</span>
                        </a>
                    </li>
                    <li<?php echo isset($menu) && $menu=='portafolio' ? ' class="active"':'' ?>>
                        <a href="/portafolio.php">
                            <span>Portafolio</span>
                        </a>
                    </li>
                    <li<?php echo isset($menu) && $menu=='quienes' ? ' class="active"':'' ?>>
                        <a href="/quienes.php">
                            <span>¿Quienes Somos?</span>
                        </a>
                    </li>
                    <li<?php echo isset($menu) && $menu=='servicios' ? ' class="active"':'' ?>>
                        <a href="/servicios.php">
                            <span>Servicios</span>
                        </a>

                        <ul>
                            <li>
                                <a href="/servicios.php#fotografia-eventos">
                                    <span>Fotografía Para Eventos</span>
                                </a>
                            </li>
                            <li>
                                <a href="/servicios.php#fotografia-documental">
                                    <span>Fotografía Documental</span>
                                </a>
                            </li>
                            <li>
                                <a href="/servicios.php#fotografia-callejera">
                                    <span>Fotografía Callejera</span>
                                </a>
                            </li>
                            <li>
                                <a href="/servicios.php#fotografia-reportajes">
                                    <span>Fotografía Para Reportajes</span>
                                </a>
                            </li>
                            <li>
                                <a href="/servicios.php#fotografia-deportiva">
                                    <span>Fotografía Deportiva</span>
                                </a>
                            </li>
                            <li>
                                <a href="/servicios.php#fotografia-naturaleza">
                                    <span>Fotografía a la Naturaleza</span>
                                </a>
                            </li>
                            <li>
                                <a href="/servicios.php#fotografia-paisajista">
                                    <span>Fotografía Paisajista</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li<?php echo isset($menu) && $menu=='contactar' ? ' class="active"':'' ?>>
                        <a href="/contactar.php">
                            <span>Contactar</span>
                        </a>
                    </li>
                </ul>

                <div id="social-icons">
                    <img src="/images/social.png" alt="iconos sociales" usemap="#socialmap" />
                    <map name="socialmap">
                        <area shape="rect" coords="0,0,30,30" target="_blank" href="http://www.facebook.com/profile.php?id=100003265333448" alt="Ir al Facebook de Photo Look" />
                        <area shape="rect" coords="45,0,75,30" target="_blank"  href="https://twitter.com/#!/PhotoLook3" alt="Ir al Twitter de Photo Look" />
                        <area shape="rect" coords="90,0,120,30" target="_blank"  href="https://plus.google.com/u/0/" alt="Ir al Perfil de Photo Look en Google+" />
                        <area shape="rect" coords="135,0,165,30"  href="mailto:photolook1@live.com" alt="Enviar un Email a Photo Look" />
                    </map>

                   
                </div>


                <div id="top-info">
                    <div>
                        <strong>Tel:</strong><span> (+51) 44 617652</span>
                    </div>
                    <div class="last">
                        <strong>Cel:</strong> <span>(+51) 948853597</span>
                    </div>
                </div>
            </div>
        </div>

        <hr/>